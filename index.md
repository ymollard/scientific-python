---
marp: true
---
<!-- 
class: invert
paginate: true
footer: 'Scientific Python – Course – Yoan Mollard – CC-BY-NC-SA – [Table of Contents](https://scientific.python.training.aubrune.eu/#2) – [Show exercises](https://scientific.python.training.aubrune.eu/exercises.html#2)'
title: Course – Scientific Python
author: 'Yoan Mollard'
image: https://www.python.org/static/favicon.ico
-->

<style>
    .hljs-string {
    color: #cd9067;
}

section {
  background-image: url('img/python-background.svg');
  background-size: cover;
}

</style>


![bg left:30% 90%](https://www.python.org/static/img/python-logo.png)

#  **Scientific Python** – Course

Yoan Mollard


---

#  CHAPTER 1

ToC

---

# CHAPTER 2

ToC


---

#  CHAPTER 3

ToC


---

#  List of mini-projects

ToC

---

